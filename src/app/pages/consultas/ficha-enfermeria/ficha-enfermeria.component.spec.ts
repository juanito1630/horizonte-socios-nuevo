import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FichaEnfermeriaComponent } from './ficha-enfermeria.component';

describe('FichaEnfermeriaComponent', () => {
  let component: FichaEnfermeriaComponent;
  let fixture: ComponentFixture<FichaEnfermeriaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FichaEnfermeriaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FichaEnfermeriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicioIndividualComponent } from './servicio-individual.component';

describe('ServicioIndividualComponent', () => {
  let component: ServicioIndividualComponent;
  let fixture: ComponentFixture<ServicioIndividualComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServicioIndividualComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicioIndividualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

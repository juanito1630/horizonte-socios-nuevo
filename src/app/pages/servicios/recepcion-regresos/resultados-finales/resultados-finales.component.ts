import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import jsPDF from 'jspdf';
import { ConsultaService } from 'src/app/services/consultas/consulta.service';
import { LaboratorioService } from 'src/app/services/consultasLab/laboratorio.service';

@Component({
  selector: 'app-resultados-finales',
  templateUrl: './resultados-finales.component.html',
  styleUrls: ['./resultados-finales.component.css']
})
export class ResultadosFinalesComponent implements OnInit {

  public paciente ={
    apellidoMaterno: '',
  apellidoPaterno: '',
callePaciente: '',
consultas:'' ,
contactoEmergencia1ApellidoMaterno: '',
contactoEmergencia1ApellidoPaterno: '',
contactoEmergencia1Curp: "",
contactoEmergencia1Edad: "",
contactoEmergencia1Genero: "",
contactoEmergencia1Nombre: "",
contactoEmergencia1Telefono: "",
contactoEmergencia2ApellidoMaterno: "",
contactoEmergencia2ApellidoPaterno: "",
contactoEmergencia2Curp: "",
contactoEmergencia2Edad: "",
contactoEmergencia2Genero: "",
contactoEmergencia2Nombre: "",
contactoEmergencia2Telefono: "",
correoPaciente: "",
correoRazonSocial2: "",
cpPaciente: '',
cpRazonSocial: "",
cpRazonSocial2: "",
curp: "",
edad: '',
estadoPaciente: "",
familia: [],
fechaNacimientoPaciente: "",
fechaRegistro: "",
genero: "",
membresiaActiva: '',
nombrePaciente: "",
nomenclaturaRegistro: "",
paisNacimiento: "",
paisPaciente: "",
paquetes: [],
paquetesPacientes: [],
razonSocial1: "",
razonSocial1Calle: "",
razonSocial1Estado: "",
razonSocial1Municipio: "",
razonSocial2: "",
razonSocial2Calle: "",
razonSocial2Estado: "",
razonSocial2Municipio: "",
razoncocial1RFC: "",
razoncocial2RFC: "",
status: "",
telefono: '',
_id: ''
  }
  





  
  public resultado =[{
    idEstudio:{
      ELEMENTOS:[{
        elementos:"",
        referencia:"",
        tipo:""
      }
      ]
    },
    obtenidos:{},
    idPaciente:[],
    idPedido:{
      _id:''
    },
    quimico:""
  }]
  
  
  public obtenido=[];
  public tipos = [];

 
  public resultadoF=[]
  public quimicoRes=[{ quimico:""}];

public id;


  constructor(

    private _laboratorio: LaboratorioService,
    private _route: ActivatedRoute,
    private _consultaService: ConsultaService 

  ) { }

  ngOnInit(): void {

    this.id = this._route.snapshot.paramMap.get('id');
    this.obtenerResultados()
  }




  obtenerResultados(){
    // console.log(this.id)

    this.paciente=JSON.parse(localStorage.getItem('idPaciente'))
    this._laboratorio.obtenerLaboratorioEvolucion(this.paciente._id)
    .subscribe(( data ) =>{ this.setEstudio( data['data'])
   console.log(data)
   
   })

  //  this.quimicoRes = this.resultado
    
  //  console.log(this.quimicoRes);
  //  console.log(this.resultado);
   
  }
 setEstudio(estudio:any){
 this.resultado = estudio

 this.resultado.forEach(element => {
  if (element.idPedido != null) {  
     if(element.idPedido._id == JSON.parse(localStorage.getItem('estudio'))){
          this.resultadoF.push(element)    
     }
  }
 });
 console.log(this.resultadoF);
 
 console.log(this.resultado);
 this.quimicoRes = this.resultado
// iteramos los resultados
for(let item of this.resultadoF){
  for(let items of item.idEstudio.ELEMENTOS){
      // se hace el push al elemento 0 del arreglo 
    if(this.tipos.length == 0){
        this.tipos.push(items.tipo);  
     }else{
       // busca los elementos dentro del arreglo

       const encontrar= this.tipos.find(element => element == items.tipo)
       if(encontrar){
          this.tipos.push("")
          // se hace un push de elementos vacios
       }else{
         // un push del elemento nuevo
         this.tipos.push(items.tipo)
       }
     }   
  }
}
console.log(this.tipos);

//  console.log(Object.keys(this.resultado[0].obtenidos));
 for(let item in this.resultadoF[0].obtenidos[0]){
   
   this.obtenido.push(this.resultadoF[0].obtenidos[0][item])
   
 }

 console.log(this.obtenido);
 
 
 }


 printComponent( tablaData ){
  let printComponents = document.getElementById(tablaData).innerHTML;
  let originalComponents = document.body.innerHTML;

  document.body.innerHTML = printComponents;
  window.print();
  document.body.innerHTML = originalComponents;
  window.location.reload();
 }


 USGpdf() {
  let showElement = document.querySelectorAll(".hide-display");
  showElement.forEach(elemento =>{
    elemento.classList.remove("hide-display");
  });
  window.print();
  window.location.reload();
}
 



}

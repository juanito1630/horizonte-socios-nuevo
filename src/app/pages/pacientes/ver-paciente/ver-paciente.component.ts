import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PacientesService } from 'src/app/services/pacientes/pacientes.service';

@Component({
  selector: 'app-ver-paciente',
  templateUrl: './ver-paciente.component.html',
  styleUrls: ['./ver-paciente.component.css']
})
export class VerPacienteComponent implements OnInit {

  // public fechaHoy =  moment().format('dddd Do MMM YYYY: hh:mm');
  public id = "";
  public paciente = {

    nombrePaciente:"",
    apellidoPaterno:"",
    apellidoMaterno: "",
    curp:"",
    telefono:0,
    consultas: 0,
    id:"",
    fechaRegistro:Date,
    genero:"",
    direccion: {
      estadoPaciente:"",
      callePaciente:"",
      paisPaciente:"",
      cp:""
    },
    contactoEmergencia1Nombre:"",
    contactoEmergencia1Edad:"",
    correo: "",
    cp:"",
    edad:"",
    paquetes : [],
    membresiaActiva:false
  }

  public paquetes: any [] = [];

  
  constructor(
    private _pacienteService: PacientesService,
    private _route: ActivatedRoute,
    public _router: Router
  ) { }
  
  ngOnInit(): void {
    this.id = this._route.snapshot.paramMap.get('id');
    this.obtenerPaciente();
    this.paquete(this.id);
  }


  obtenerPaciente(){

    this._pacienteService.getPacienteBtID(  this.id )
    .subscribe( (data:any) => {

      this.paciente.nombrePaciente = data.paciente.nombrePaciente;
      this.paciente.apellidoPaterno = data.paciente.apellidoPaterno;
      this.paciente.apellidoMaterno = data.paciente.apellidoMaterno;
      this.paciente.consultas = data['paciente'].consultas;
      this.paciente.cp = data.paciente.cpPaciente;
      this.paciente.direccion.estadoPaciente = data['paciente']['estadoPaciente'];
      this.paciente.direccion.paisPaciente = data['paciente']['paisPaciente'];
      this.paciente.direccion.callePaciente = data['paciente']['callePaciente'];
      this.paciente.direccion.cp = data['paciente']['cpPaciente'];
      this.paciente.edad = data.paciente.edad;
      this.paciente.correo = data.paciente.correoPaciente;
      this.paciente.curp = data.paciente.curp;
      this.paciente.id = data.paciente._id;
      this.paciente.paquetes = data.paciente.paquetes;
      this.paciente.contactoEmergencia1Nombre = data['paciente']['contactoEmergencia1Nombre'];
      this.paciente.contactoEmergencia1Edad = data['paciente']['contactoEmergencia1Edad'];
      this.paciente.genero = data['paciente']['genero'];
      this.paciente.fechaRegistro = data['paciente']['fechaRegistro'];
      // console.log( this.paciente );
      console.log( data )
      this.guardarLocalStorage();
    });
  }


  guardarLocalStorage(){

    if(  localStorage.getItem('paciente') ){
  
      localStorage.removeItem('paciente');
    }
    if(  this.paciente.paquetes.length >= 1 || this.paciente.membresiaActiva == true ){
        this.paciente.membresiaActiva = true;
    }
    
    localStorage.setItem('paciente', JSON.stringify(this.paciente));
  }

  irAUnServicio(  servicio ){
    this.setRouteService( servicio );
  }

  setRouteService(servicio){
    this._router.navigate([ `/serviciosInt/${servicio}`]);
  }

  paquete(id){
    this._pacienteService.getPaquetePaciente(id).subscribe(
      (data: any) => {
          // console.log( data );
          this.paquetes = data['data'];
      },
      err => {
        console.log(<any>err);
    });  
  }

}

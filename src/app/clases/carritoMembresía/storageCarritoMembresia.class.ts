
import CarritoMembresiaStorage from '../../interfaces/carrito.interfaces';

export default class StorageCarritoMembresia {

    public carritoJson = {
        items:[],
        total:0
    };

    constructor() {
    }

    public carritoGuardado :CarritoMembresiaStorage;

    guardarCarritoStorage(  carrito: CarritoMembresiaStorage ){

        let carritoString = JSON.stringify(  carrito );
         localStorage.setItem('carritoMembresia',  carritoString );
    }

    obtenerCarritoMembresia(){
        this.carritoJson  = JSON.parse(  localStorage.getItem('carritoMembresia') );
        return this.carritoJson;
    }
    
    eliminarStorageCarritoMembresia(){
        localStorage.removeItem('carritoMembresia');
        return true;  
    }

}
import { Component, OnInit,Input} from '@angular/core';

import { ConsultaService } from 'src/app/services/consultas/consulta.service';
import { RecetasService } from 'src/app/services/recetas/recetas.service';



@Component({
  selector: 'app-tablas-consultas',
  templateUrl: './tablas-consultas.component.html',
  styleUrls: ['./tablas-consultas.component.css']
})
export class TablasConsultasComponent implements OnInit {

  @Input() estudiosPendientes: string;

  public consultas:[]=[];
  public recetasConEstudios:[] =[];
  public pagina = 0;
  public total: string;

  constructor(
    public _consultaService: ConsultaService,
    private _recetaService: RecetasService
  ) { }

  ngOnInit(): void {

    if( this.estudiosPendientes == 'consultas'){
      this.obtenerCosnultas();
      console.log('entro ya a estudiuos');
      

    }else if( this.estudiosPendientes == 'estudios'  ){
      this.obtenerRecetas();

    }

  }
  obtenerCosnultas(){
    this._consultaService.verConsultasRecepcion()
    .subscribe( (data) =>   {
      console.log(data);
      this.consultas = data['data'].reverse()
      this.total=data['data'].results;
    });
  }


  obtenerRecetas(){
    this._recetaService.verRecetasEmitidas(  )
    .subscribe(  (data) => {
      console.log(data)
      this.recetasConEstudios = data['data'].reverse();
    });
  }

}

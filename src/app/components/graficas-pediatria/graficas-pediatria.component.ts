import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { HistoriaClinicaService } from 'src/app/services/historiaClinica/historia-clinica.service';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { ConsultaService } from 'src/app/services/consultas/consulta.service';
import { WsLoginService } from 'src/app/services/sockets/ws-login.service';
import { ActivatedRoute, Router } from '@angular/router';



@Component({
  selector: 'app-graficas-pediatria',
  templateUrl: './graficas-pediatria.component.html',
  styleUrls: ['./graficas-pediatria.component.css']
})
export class GraficasPediatriaComponent implements OnInit {

  @Input() idConsulta : string;
  // @Input() signosVitalestl = {
    
  // }
  @Input()  paciente  = {
    nombre: '',
    apellidoPaterno: '',
    apellidoMaterno: '',
    estadoPaciente: '',
    fechaNacimiento: '',
    telefono: '',
    edad: 0,
    genero: '',
    curp:'',
    callePaciente:'',
    cpPaciente:'',
    paisPaciente:'',
    idMedicinaPreventiva: '',
    idAntecedentesHeredoFam: '',
    peso: 0,
    talla: 0,
    imc: 0,
    pc: 0,
    idPaciente:''
  };

  public edadMeses25 = "";
  public id = "";

  public edad = 0;
  public talla = 0;
  public peso = 0;
  public imc = 0;
  public pc = 0;

  public signosVitales = {
    edad: 0,
    talla: 0,
    peso: 0,
    imc: 0,
    pc: 0
  }
  constructor(
    private _HistoriaClinicaService: HistoriaClinicaService,
    private _WsLoginService: WsLoginService,
    private _consultaService: ConsultaService,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit(): void {
    // este id es de la consulta
    this.id = this._route.snapshot.paramMap.get('id');
    // console.log('Holiiiiii: ',this.id);
 
    
  }


  setPeso(){
    this.signosVitales.peso = this.peso;
    this.signosVitales.talla = this.talla;
    this.signosVitales.imc = this.imc;
    this.signosVitales.pc = this.pc;
  }
  
 
  
obtenerIMC(){

  this.paciente.imc = ((this.paciente.peso)/(this.paciente.talla * this.paciente.talla));
  let imcL = this.imc.toString();

  imcL.split(',', 2);

  let imcLn;
  imcLn = parseFloat(imcL).toFixed(2);

  this.paciente.imc = imcLn;
  
  // console.log('IMC:', this.paciente.imc );
  
  this.recorrerEdadNinaPeso();
  this.recorrerEdadNinaTalla();
  this.recorrerEdadNinaIMC();
  this.recorrerAlturaCentimetrosNinas();
  }
  obtenerIMCDos(){

    this.imc = ((this.peso)/(this.talla * this.talla));
    let imcL = this.imc.toString();
  
    imcL.split(',', 2);
  
    let imcLn;
    imcLn = parseFloat(imcL).toFixed(2);
  
    this.imc = imcLn;
    
    // console.log('IMC:', this.imc );
    
    this.recorrerEdadNinaPeso();
    this.recorrerEdadNinaTalla();
    this.recorrerEdadNinaIMC();
    this.recorrerAlturaCentimetrosNinas();
    }

 
  updateSV(){
    this._consultaService.actualizarSignosVitalesEnElPaciente(this.id, this.paciente)
    .subscribe((data:any) => {
      if(data['ok']) {
        alert('Se actualizaron los Signos Vitalestl')
        // console.log(data);
        
      }
    })
  }


  ageragrSignos(){

    this.setPeso();
  
    this._HistoriaClinicaService.agregarSignosVitales( this.idConsulta,  this.signosVitales )
    .subscribe(  (data:any) => {
        // console.log(data);
      if(  data['ok']){

          alert('Signos vitales guardados');
          // this.cambioDeEstadoCosnulta();
          this._WsLoginService.notificarDoctor(data);
      }
  
     });
  
  }

  recorrerEdadMesesNinaPC(){
    let edadMeses = this.paciente.edad * 12;
    // let transform = parseFloat(this.pc.toString())
    // transform = this.pc;
    // console.log(edadMeses);
    
    for(let i=0; i < edadMeses; i++){
      this.chartCefalicoNinas[2].data.push(0);


      if(this.chartCefalicoNinas[2].data.length == edadMeses){

        this.chartCefalicoNinas[2].data.push( this.pc );
        // console.log(this.chartCefalicoNinas[2]);
        
      }
    }
  }

  
  public chartCefalicoNinas: ChartDataSets[] = [
    { data: [35.48, 36.98, 38.13, 39.04, 39.79, 40.43, 40.97, 41.44, 41.86, 42.22, 42.55, 42.84, 43.10, 43.34, 43.55, 43.75, 43.93, 44.10, 44.25, 44.39, 44.51, 44.63, 44.74, 44.84, 44.94, 45.02, 45.10, 45.18, 45.24, 45.31, 45.37, 45.39, 45.42, 45.43, 45.44, 45.45, 45.50], label: 'P.C. Mínimo'},
    { data: [38.12, 40.97, 42.22, 43.20, 44.00, 44.68, 45.26, 45.77, 46.23, 46.64, 47.01, 47.34, 47.65, 47.94, 48.20, 48.45, 48.68, 48.90, 49.10, 49.30, 49.48, 49.65, 49.81, 49.97, 50.12, 50.26, 50.39, 50.52, 50.65, 50.77, 50.88, 50.99, 51.01, 51.02, 51.03, 51.10, 51.20], label: 'P.C. Máximo'},
    { data: [], label: 'P.C. Actual'}
  ];

  public chartPesoPorTallaNinas: ChartDataSets[] = [
    {data: [0, 0, 8.83, 9.23, 9.63, 10.03, 10.42, 10.83, 11.23, 11.64, 12.06, 12.49, 12.93, 13.38, 13.84, 14.32, 14.81, 15.33, 15.87, 16.43, 17.02, 17.65, 18.32, 19.03, 19.79], label: 'Mínimo'},
    {data: [0, 0, 12.03, 12.51, 12.99, 13.49, 14.01, 14.56, 15.14, 15.76, 16.42, 17.13, 17.90, 18.72, 19.59, 20.51, 21.47, 22.47, 23.51, 24.56, 25.61, 26.66, 27.67, 28.62, 29.49], label: 'Máximo'},
    {data: [], label: 'Acual'}
  ];
  public pesoPorTallaNinasEjeX: Label[] = ['73', '75', '77', '79', '81', '83', '85', '87', '89', '91', '93', '95', '97', '99', '101', '103', '105', '107', '109', '111', '113', '115', '117', '119', '121', '123', '125', '127', '129', '131', '133', '137', '139'];

  recorrerAlturaCentimetrosNinas(){
    let alturaCentimetros  = this.talla * 100;
    let pesoAltura = this.peso;
    
   
    
    
    for(let i=73; i < alturaCentimetros; i+=2){
      
      
      this.chartPesoPorTallaNinas[2].data.push(0); 
      
      // this.chartPesoPorTallaNinas[2].data.push(0);
      // console.log('Peso', pesoAltura);
      // console.log('Altura', alturaCentimetros);
      // console.log('Tamaño del Array', this.chartPesoPorTallaNinas[2].data.length);
      // console.log(this.chartPesoPorTallaNinas[2].data.length == this.peso);
      
      
      var tArray = this.chartPesoPorTallaNinas[2].data.length;
       
      }
      if(this.chartPesoPorTallaNinas[2].data.length == tArray){
  
        this.chartPesoPorTallaNinas[2].data.push( pesoAltura );
        // console.log(this.chartCefalicoNinas[2]);
        
      
      }

  }



  
  public lineChartData: ChartDataSets[] = [
    { data: [10.673, 11.633, 13.291, 14.990, 16.500, 17.900, 19.900, 22.076, 24.380, 26.562, 29.472, 32.600, 36.712, 41.149, 45.459, 49.866, 51.69, 53.148, 54.200], label: 'Peso Mínimo' },
    { data: [15.668, 18.045, 20.739, 24.461, 28.271, 32.149, 36.983, 39.666, 46.566, 48.863, 56.260, 63.308, 69.705, 76.420, 82.735, 88.504, 93.779, 97.245, 99.185, 102.300], label: 'Peso Máximo' },
    { data:[ ], label: 'Peso Actual'},

  ];

  // Grafica de estatura :D
  public lineChartEstatura: ChartDataSets [] = [
    { data: [.80, .84, .88, .96, 1.02 , 1.08, 1.15, 1.20, 1.26, 1.30, 1.33, 1.36, 1.43, 1.45, 1.50, 1.57, 1.58, 1.60, 1.62, 1.63, 1.66], label: 'Estatura Mínima'},
    { data: [.93, 1.03, 1.12, 1.20, 1.28, 1.33, 1.36, 1.44, 1.50, 1.54, 1.57, 1.64, 1.70, 1.73, 1.76, 1.80, 1.83, 1.85, 1.88, 1.89, 1.90], label: 'Estatura Máxima'},
    { data: [ ], label: 'Estatura Actual'}
  ];

  // Grafica de IMC
  public lineChartIMC: ChartDataSets [] = [
    { data: [14.5, 14.09, 13.81, 13.64, 13.54, 13.52, 13.57, 13.78, 13.96, 14.28, 14.68, 15.10, 15.66, 16.16, 16.77, 17.28, 17.85, 18.30, 19.21], label: 'IMC Mínimo'},
    { data: [19.66, 18.69, 18.23, 18.44, 19.10, 20.08, 21.23, 22.37, 23.72, 24.92, 26.03, 27.02, 27.88, 28.63, 29.30, 29.88, 30.61, 31.20, 32.10], label: "IMC Máximo"},
    { data: [ ], label: "IMC Actual"}

  ];

  recorrerEdadNinaTalla(){
    let edadEscala = this.paciente.edad;

    for(let i=0; i < edadEscala; i++){
      this.lineChartTallaNina[2].data.push(0);


      if(this.lineChartTallaNina[2].data.length == edadEscala){
        this.lineChartTallaNina[2].data.push( this.talla);
      }
    }
  }

  recorrerEdadNinaPeso(){
    let edadEscala = this.paciente.edad;

    for(let i=0; i < edadEscala; i++){
      this.lineChartPesoNina[2].data.push(0);


      if(this.lineChartPesoNina[2].data.length == edadEscala){
        this.lineChartPesoNina[2].data.push( this.peso);
      }
    }
  }

  recorrerEdadNinaIMC(){
    let edadEscala = this.paciente.edad;

    for(let i=0; i < edadEscala; i++){
      this.lineChartIMCNina[2].data.push(0);


      if(this.lineChartIMCNina[2].data.length == edadEscala){
        this.lineChartIMCNina[2].data.push( this.imc);
      }
    }
  }
  
  public lineChartTallaNina: ChartDataSets [] = [
    { data: [.78, .86, .94, 1.01, 1.08, 1.14, 1.20, 1.24, 1.30, 1.38, 1.45, 1.48, 1.50, 1.56, 1.59, 1.61, 1.6123, 1.621, 1.625, 1.63], label: 'Estatura Mínima'},
    { data: [.92, 1.01, 1.11, 1.20,  1.28, 1.36, 1.43, 1.50, 1.57, 1.65, 1.71, 1.73, 1.74, 1.7526, 1.7536, 1.78, 1.80, 1.81, 1.817], label: 'Estatura Máxima'},
    { data: [], label: 'Estatura Actual' }
    
  ];

  public lineChartPesoNina: ChartDataSets [] = [
    { data: [10.17, 11.38, 12.65, 14.34, 16.01, 17.72, 19.64, 21.58, 23.99, 26.81, 29.74, 33.12, 36.70, 39.37, 41.82, 43.23, 44.24, 45.01, 45.12], label: 'Peso Mínimo'},
    { data: [15.35, 18.0, 21.28, 24.62, 28.92, 33.36, 38.07, 44.58, 51.42, 58.10, 65.32, 71.87, 77.68, 81.36, 84.36, 86.04, 87.34, 88.15, 88.20], label: 'Peso Máximo'},
    { data: [ ], label: 'Peso Actual' }
  ];

  public lineChartIMCNina: ChartDataSets [] = [
    { data: [14.07, 13.77, 13.51, 13.33, 13.22, 13.22, 13.27, 13.44, 13.76, 14.11, 14.53, 14.94, 15.44, 15.97, 16.37, 16.86, 17.97, 18.33, 18.35], label: 'IMC Mínimo'},
    { data: [19.51, 18.70, 18.61, 18.99, 19.67, 20.78, 21.87, 23.27, 24.50, 25.80, 27.05, 28.22, 29.31, 30.30, 31.30, 32.11, 32.99, 33.1, 33.19], label: 'IMC Máximo'},
    { data:  [ ], label: 'IMC Actual' }
  ];
  public lineChartLabels: Label[] = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18'];

  // Eje de las X
  public lineChartEjeX: Label[] = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18'];

  public cefalicoNinasEjeX: Label[] = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36'];

  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        },
        {
          id: 'y-axis-1',
          position: 'right',
          gridLines: {
            color: 'rgba(0,0,2555,0.2)',
          },
          ticks: {
            fontColor: 'black',
          }
        }
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
  };
  public lineChartColors: Color[] = [
    { // Rosa Bajito
      // backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: '#ea3af0',
      // pointBackgroundColor: '#ea3af0',
      // pointBorderColor: '#ea3af0',
      // pointHoverBackgroundColor: '#fff',
      // pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      // backgroundColor: 'rgba(0,0,255,1)',
      borderColor: ' #f40bfc',
      // pointBackgroundColor: ' #f40bfc',
      // pointBorderColor: ' #f40bfc',
      // pointHoverBackgroundColor: '#fff',
      // pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // red
      // backgroundColor: 'rgba(0,255,0,0.6)',
      borderColor: '#fc0b90',
      // pointBackgroundColor: 'rgb(241, 29, 171)',
      // pointBorderColor: 'green',
      // pointHoverBackgroundColor: 'green',
      // pointHoverBorderColor: 'rgba(0,255,0,0.6)'
    }
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [];

  @ViewChild (BaseChartDirective, { static: true }) chart: BaseChartDirective;

  //Graficas Funciones
  //Inicio Funciones Grafica
public randomize(): void {
  for (let i = 0; i < this.lineChartData.length; i++) {
    for (let j = 0; j < this.lineChartData[i].data.length; j++) {
      this.lineChartData[i].data[j] = this.generateNumber(i);
    }
  }
  this.chart.update();
}

private generateNumber(i: number) {
  return Math.floor((Math.random() * (i < 2 ? 100 : 1000)) + 1);
}

// events
public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
  // console.log(event, active);
}

public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
  // console.log(event, active);
}

public hideOne() {
  const isHidden = this.chart.isDatasetHidden(1);
  this.chart.hideDataset(1, !isHidden);
}

public pushOne() {
  this.lineChartData.forEach((x, i) => {
    const num = this.generateNumber(i);
    const data: number[] = x.data as number[];
    data.push(num);
  });
  this.lineChartLabels.push(`Label ${this.lineChartLabels.length}`);
}

public changeColor() {
  this.lineChartColors[2].borderColor = 'green';
  this.lineChartColors[2].backgroundColor = `rgba(0, 255, 0, 0.3)`;
}

public changeLabel() {
  this.lineChartLabels[2] = ['1st Line', '2nd Line'];
  // this.chart.update();
}
//fin graficas funciones


}

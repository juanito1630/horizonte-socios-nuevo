import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PaquetesService } from 'src/app/services/paquetes/paquetes.service';
import { FichaInfo } from '../../../clases/ficha-info-paciente';


@Component({
  selector: 'ficha-info-user',
  templateUrl: './ficha-info-user.component.html',
  styleUrls: ['./ficha-info-user.component.css']
})
export class FichaInfoUserComponent implements OnInit {

  @Input() pacienteInfo: FichaInfo;
  @Input() title: true;
  public id = ""
  public fecha:Date;

  // public pacienteInfo = {
  //   nombrePaciente :"",
  //   apellidoPaterno: "",
  //   apellidoMaterno: "",
  //   curp: "",
  //   edad: 0,
  //   genero: "",
  //   _id:"",
  //   callePaciente: "",
  //   fechaNacimientoPaciente:"",
  //   estadoPaciente: "",
  //   paisPaciente: "",
  //   telefono: "",
  // };

  
  
  constructor(
    // private _route: ActivatedRoute,
    public _paquete:PaquetesService
  ) { 

  }

  ngOnInit(){
    // this.id = this._route.snapshot.paramMap.get('id');
    // this.obtenerPacienteInfo();
    console.log( this.pacienteInfo );
  }


  // obtenerPacienteInfo(){
  //   this._paquete.obtenerPaquete(  this.id )
  //   .subscribe( (data:any) => {

  //     // this.pacienteInfo = data['paquetes'][0]['paciente'];
    

  //   });
  // }


}

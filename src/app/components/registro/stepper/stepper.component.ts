import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { LocationServiceService } from 'src/app/services/otrosService/location-service.service';
import { PacientesService } from 'src/app/services/pacientes/pacientes.service';
import swal from 'sweetalert'
import {  MatStepper  } from '@angular/material/stepper';
import { splitAtColon } from '@angular/compiler/src/util';
// import { NgStepperModule } from 'angular-ng-stepper';


@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.css']
})
export class StepperComponent implements OnInit {

  public estados: [];
  public paises:[];
  public municipios:[];
  public estado: string;
  public pais: string;
  public paquetes: [];
  public validateBtn = false;
  public edadPaciente: HTMLInputElement;

  
  constructor(
    public _locationService: LocationServiceService,
    public _pacienteService: PacientesService,
    private _router: Router
  ) { }

  ngOnInit(): void {
    this.obtenerPaises();
    //this.obtenerEstados();
    // this.obtenerPaquetes();
  }

  obtenerPaises(){

    this._locationService.getCountries()
    .subscribe(
      (data:any) => {  
        //mapeamos los datos obtenidos de la api y extraemos los paises
        let estructurar_paises = data.map(paises=>paises.country);
        this.paises = estructurar_paises;
      });
  }

  obtenerEstados(pais){
    this._locationService.getEstado(pais)
    .subscribe((data:any) => {
      //mapeamos los datos obtenidos de la api y extraemos los nombres de los estados
        let estructurar_estados = data.map(paises=>paises.name);
        this.estados = estructurar_estados
      });
  }

  ObtenerMunicipio(estado){
    this._locationService.getMunicipios(this.pais,estado)
    .subscribe(
      (data:any) => {
        this.municipios = data
      });
  }

  buscarMunicipios() {
    this.ObtenerMunicipio( this.estado )
  }

  buscarEstados() {
    this.obtenerEstados( this.pais );
  }

  validarFecha(fechaForm: HTMLInputElement){
    let fechaElegida = fechaForm.value;
    let fechaAhora = new Date();
    if(Date.parse(fechaElegida.toString()) > Date.parse(fechaAhora.toString())){
      fechaForm.value = fechaAhora.toString();
    }
  }

  generarEdad(edadForm: HTMLInputElement ) {
    this.edadPaciente = document.querySelector('#edad');

    // console.log( edadForm.value );
    let fecha = edadForm.value;
    let splitFecha = fecha.split('-');
    var fechaActual = new Date();
    var anio = fechaActual.getFullYear();

    let edadNormal = anio - parseFloat( splitFecha[0]  );
    let edades = edadNormal.toString();
    this.edadPaciente.value = edades;
    this.validarFecha(edadForm);
  }
  //Comienzan los metodos de validaciones
  validarCurp( curp: string  ){
    if(  curp.length == 18  ){
      return true;
    }else{
      return false;
    }
  }

  validarString(nombre: string){
    if( nombre == ""  || nombre.length <3 ){
        return false;
    }
    return true;
  }
  validarSexo( sexo: string ){
    if(  sexo.length < 4 || sexo == "" ){
      return false;
    }else {
      return true;
    }
  }
  validarNumeros(numero:any){
    if( numero === "" ){
      return false
    }else{
      if( isNaN(numero ) ){
        return false;
      }else {
        return true;
      }
    }
    
  }
  validarCorreo(correo:string){
    //validamos el correo electronico con una expresion regular.
    let regex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if(regex.test(correo)){
      return true;
    }else{
      return false
    }
  }

  validarEdad(edad:any){
    //obtenemos el objectInputHTML y lo guardamos en una variable y comparamos su valor.
    let edadPac = edad;
    if( edadPac.value <= 0 || edadPac.value > 110  ){
      return false;
    }else{
      this.edadPaciente.value = edadPac.value;
      return true;
    }
  }
  validarFechaNacimiento(date:Date){
    let fechaAhora = new Date();
    if(Date.parse(date.toString()) > Date.parse(fechaAhora.toString())){
      return false
    }else{
      return true;
    }
  }

  message(msj){
    swal(msj,{icon:"warning"})
    /* alert(msj); */
  }

  validaciones(form){
    //Validaciones del paciente
    if( this.validarString( form.nombrePaciente ) ){

    }else {
      this.message('Completa el nombre del paciente')
      return false;
    }

    if( this.validarString( form.apellidoPaterno ) ){

    }else {
      this.message('Completa el apellido paterno del paciente')
      return false;
    }

    if( this.validarString( form.apellidoMaterno ) ){

    }else {
      this.message('Completa el apellido materno del paciente')
      return false;
    }

    if( this.validarCurp( form.curp ) ){

    }else {
      this.message('Ingresa un curp valido')
      return false;
    }

    if( this.validarString( form.lugarOrigen || "" ) ){
    
    }else {
      this.message('Completa el Lugar de origen del paciente')
      return false;
    }
      
    if( this.validarFechaNacimiento( form.fechaNacimiento || "" ) ){
    
    }else {
      this.message('Ingresa una fecha valida de nacimiento del paciente')
      return false;
    }
      //enviamos el valor del campo de Edad
    if( this.validarEdad( document.querySelector('#edad') || 0 ) ){

    }else {
      this.message('Ingrea una edad valida')
      return false;
    }

    if( this.validarNumeros( form.telefono || "" ) ){
    
    }else {
      this.message('Completa el telefono del paciente')
      return false;
    }

    if ( this.validarSexo( form.genero ) ){

    }else {
      this.message('Ingresa el sexo del paciente');
      return false;
    }

    if( this.validarCorreo( form.correoPaciente ) ){
    
    }else {
      this.message('Completa el correo del paciente')
      return false;
    }

    if( this.validarString( form.callePaciente ) ){
    
    }else {
      this.message('Completa la calle del paciente')
      return false;
    }

    if( this.validarNumeros( form.codigoPostal || "" ) ){
    
    }else {
      this.message('Completa el Codigo postal del paciente')
      return false;
    }

    if( this.validarString( form.paisActual || "" ) ){
    
    }else {
      this.message('Completa el País actual del paciente')
      return false;
    }

    if( this.validarString( form.estadoPaciente || "" ) ){
    
    }else {
      this.message('Completa el Estado del paciente')
      return false;
    }

    if( this.validarString( form.municipioPaciente ) ){
    
    }else {
      this.message('Completa el Municipio del paciente')
      return false;
    }
    if( this.validarNumeros( form.expediente ) ){
    
    }else {
      this.message('Completa el número de expediente del paciente')
      return false;
    }
    //Validaciones contacto de emergencia
    if( this.validarString( form.contactoEmergencia1Nombre ) ){
    
    }else {
      this.message('Completa el nombre del contacto de emergencia')
      return false;
    }

    if( this.validarString( form.contactoEmergencia1ApellidoPaterno ) ){
    
    }else {
      this.message('Completa el apellido paterno del contacto de emergencia')
      return false;
    }  

    if( this.validarString( form.contactoEmergencia1ApellidoMaterno ) ){
    
    }else {
      this.message('Completa el apellido materno del contacto de emergencia')
      return false;
    }  

    if( this.validarString( form.contactoEmergencia1Parentesco ) ){
    
    }else {
      this.message('Completa el parentesco del contacto de emergencia')
      return false;
    }  
    
    if( this.validarEdad( document.querySelector("#contactoEmergencia1Edad") || 0 ) ){

    }else {
      this.message('Ingrea una edad valida del contacto de emergencia')
      return false;
    }

    if( this.validarNumeros( form.contactoEmergencia1Telefono || "" ) ){
    
    }else {
      this.message('Completa el telefono del contacto de emergencia')
      return false;
    }

    if ( this.validarSexo( form.contactoEmergencia1Genero ) ){

    }else {
      this.message('Ingresa el sexo del contacto de emergencia');
      return false;
    }
    //  this.validateBtn = true; contactoEmergencia1Parentesco
    return true;

  }

  enviar( form: NgForm  ){
    form.value.edad = this.edadPaciente.value
    let resultado =this.validaciones( form.value );
    // console.log( form.value );
    if(  resultado ){
      this.validateBtn = true;
      this._pacienteService.setPacientes(   form.value , 'CTLA01'  )
      .subscribe((data) => {
        
        if(  data['ok'] ){
          swal("Paciente registrado",{icon:"success"})
          /* alert('Paciente registrado'); */
          this._router.navigateByUrl('/paciente')
      
        }
      });

    }else {

      return;

    }
  
  }

}

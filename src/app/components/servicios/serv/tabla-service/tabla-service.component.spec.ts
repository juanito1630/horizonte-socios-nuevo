import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaServiceComponent } from './tabla-service.component';

describe('TablaServiceComponent', () => {
  let component: TablaServiceComponent;
  let fixture: ComponentFixture<TablaServiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TablaServiceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

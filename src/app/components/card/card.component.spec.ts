import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ModulesService } from 'src/app/services/modules/modules.service';

import { CardComponent } from './card.component';

describe('CardComponent', () => {
  let component: CardComponent;
  let fixture: ComponentFixture<CardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardComponent ],
      providers: [ ModulesService ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Debe de regresar modulos desde la DB', () => {

    const modules = component.getModuleByRole();
    expect( component.modules.length ).toBeGreaterThan( 0);


  });


});

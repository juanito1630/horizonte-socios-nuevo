import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL } from 'src/app/config/conf';

@Injectable({
  providedIn: 'root'
})
export class PagosService {
  public url = URL;

  constructor( private _http: HttpClient  ) { }

  public agregarPedido(venta){

    let uri  =  `${this.url}/venta/servicios`;
    return this._http.post( uri, venta );

  }

  public venta(venta){
    let url = `${this.url}/nueva/compra/sede`;
    return this._http.post( url, venta );
  }

  //  sin registro del paciente
  // public guardarVentaSinRegistro( venta ){
  //   let url = `${this.url}/venta/sin/registro`;
  //   return this._http.post( url, venta );
  // }

  pedidosLaboratorio( pedido  ){
    let url = `${this.url}/crear/pedido`;
    return this._http.post( url, pedido  );
  }

  buscarPorFechasPagos( body ){
    console.log( body);
    const url = `${this.url}/ver/pedidos/pagados/fecha/sede`;
    return this._http.post( url, body );
  }
}

